import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../apiservice.service';
@Component({
  selector: 'app-apidata',
  templateUrl: './apidata.component.html',
  styleUrls: ['./apidata.component.css']
})
export class ApidataComponent implements OnInit {

  constructor(private apiserve:ApiserviceService) { }
  data:any;
  arr:any;
  btnText:string = "click here to see data";

  ngOnInit(): void {
    Array(this.apiserve.getData().subscribe(data=>{
      console.log(data);
      this.arr = data;
    }));
  }

  assignData():void{
    this.data =this.data===undefined?this.arr:undefined;  
    this.btnText = this.btnText === "click here to hide text"?"click here to see data":"click here to hide text"; 
  }

}
