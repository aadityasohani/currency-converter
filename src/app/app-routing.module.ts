import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApidataComponent } from './apidata/apidata.component';
import { CurrencyComponent } from './currency/currency.component';

const routes: Routes = [
  {path:"currency",component:CurrencyComponent},
  {path:"apiData",component:ApidataComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
