import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../apiservice.service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.css']
})
export class CurrencyComponent implements OnInit {

  inpCurr:number = 0;
  inputCurr:string = "inr";

  optCurr:number = 0;
  outputCurr:string="inr";


  constructor(private apiserve:ApiserviceService) { }

  logit():void{
    this.apiserve.log();
  }

  ngOnInit(): void {
    
  }

  inRupee:number = this.inpCurr;

  giveChange():void{
    switch(this.inputCurr){
      case 'usd':
        this.inRupee = 74.77*this.inpCurr;
        break;
      case 'euro':
        this.inRupee = 84.34*this.inpCurr;
        break;
      case 'din':
        this.inRupee = 247.5*this.inpCurr;
        break;
      case 'cd':
        this.inRupee = 59.11*this.inpCurr;
        break;
      default:
        this.inRupee = this.inpCurr;
        break;
    }


    switch(this.outputCurr){
      case "usd":
        this.optCurr=this.inRupee/74.77;
        break;
      case "euro":
        this.optCurr =this.inRupee/84.34;
        break;
      case "din":
        this.optCurr = this.inRupee/247.5;
        break;
      case "cd":
        this.optCurr = this.inRupee/59.19;
        break;
      default:
        this.optCurr=this.inRupee;
        break
    }
  }


}
